// MazeGenerator.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <iostream>
#include <utility>
#include <chrono>
#include <functional>
#include <random>
#include <stack>
#include <vector>


int m_iHeight = 31, m_iWidth = 31;
char** m_aacFelder;

void GenMaze();

int main() {
	m_aacFelder = new char*[m_iWidth];
	for (int i = 0; i < m_iWidth; ++i) {
		m_aacFelder[i] = new char[m_iHeight];
		for (int j = 0; j < m_iHeight; ++j) {
			m_aacFelder[i][j] = 'M';
		}
	}

	//Generieren
	GenMaze();

	for (int i = 0; i < m_iHeight; ++i) {
		for (int j = 0; j < m_iWidth; ++j) {
			std::cout << m_aacFelder[j][i] << ' ';
		}
		std::cout << std::endl;
	}

	system("PAUSE");

    return 0;
}

void GenMaze() {
	//Richtungen, in denen das n�chste Feld liegen kann
	std::pair<int, int> directions[4] = { {2,0},{0,2},{-2,0},{0,-2} };

	//Randomdistributoren f�r die koordinaten
	std::default_random_engine rGenerator(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<int> distX(1, m_iWidth - 2);
	std::uniform_int_distribution<int> distY(1, m_iHeight - 2);

	std::uniform_int_distribution<int> directionDist(0, 3);


	//Startfeld muss ungerade koordinaten haben & im richtigen bereich liegen
	int startX = distX(rGenerator);
	int startY = distY(rGenerator);
	startX = startX - startX % 2 + 1;
	startY = startY - startY % 2 + 1;

	//einfacherer zugriff
	auto randOrd = std::bind(directionDist, rGenerator);

	//Startfeld -> stack
	std::stack<std::pair<int, int>> backJump;
	backJump.push(std::pair<int, int>(startX, startY));
	m_aacFelder[startX][startY] = ' ';

	while (!backJump.empty()) {
		auto pCur = backJump.top();
		backJump.pop();

		int iDirStartIndex = randOrd();
		//Kann einer der vier nachbarn des aktuellen feldes als Gang gesetzt werden?
		for (int i = 0; i < 4; ++i) {
			int iCurIndex = (iDirStartIndex + i) % 4;
			int iNewXCoord = pCur.first + directions[iCurIndex].first;
			int iNewYCoord = pCur.second + directions[iCurIndex].second;

			//Array-out-of-bounds verhindern f�r randfelder
			if (0 < iNewXCoord && iNewXCoord < m_iWidth - 1
				&& 0 < iNewYCoord && iNewYCoord < m_iHeight - 1) {
				//Ist das zu untersuchende Feld noch kein gesetzter Gang? -> zum Gang machen
				if (m_aacFelder[iNewXCoord][iNewYCoord] != ' ') {
					m_aacFelder[iNewXCoord][iNewYCoord] = ' ';
					m_aacFelder[pCur.first + directions[iCurIndex].first / 2][pCur.second + directions[iCurIndex].second / 2] = ' ';

					backJump.push(pCur);
					backJump.push(std::pair<int, int>(iNewXCoord,iNewYCoord));
					break;
				}
			}
		}
	}
}