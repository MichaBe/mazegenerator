# Projekt Maze-Generator (Ende 2016)

Für ein Spiel, das ich im Rahmen meines Studiums programmierte, schrieb ich einen Maze-Generator. Die Ausgabe sollte ein zweidimensionales Array sein, auf dem die Mauern und Wege erkennbar sind. Alle Wege sollen miteinander verbunden sein, sodass Start und Ende auf zufällige Wegfelder gesetzt werden können.

## Verwendete Technologien

* C++
* Standard Template Library

## Kurzbeschreibung

Der Algorithmus startet mit einem Array, das mit Mauern gefüllt ist. Von einem zufälligen Startpunkt ausgehend werden Wege in das Array gezeichnet. Stößt der Algorithmus dabei auf ein Feld, das bereits ein Weg ist, geht er solange zurück, bis er eine Kreuzung findet, von der aus ein neuer Weg gezeichnet werden kann. Diese Vorgehensweise wird wiederholt, bis das komplette Array entsprechend gefüllt ist.

## Weiterführende Ressourcen

Visualisierung: [YouTube](https://www.youtube.com/watch?v=S5T9YS7HBto) (zuletzt aufgerufen: Oktober 2018)
